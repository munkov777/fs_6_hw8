import java.util.HashSet;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;
    public void addPet(int indexOfFamily, Pet pet) {
        HashSet<Pet> petHashSet = new HashSet<>();
        if (families.get(indexOfFamily).getPetsSet() == null){
            families.get(indexOfFamily).setPetsSet(petHashSet);
        }
        families.get(indexOfFamily).getPetsSet().add(pet);
    }

    @Override
    public List getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return false;
    }

    @Override
    public void saveFamily(Family family) {

    }
}
