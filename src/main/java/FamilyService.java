import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;


public class FamilyService {

    private CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public CollectionFamilyDao getCollectionFamilyDao() {
        return collectionFamilyDao;
    }

    public void setCollectionFamilyDao(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    public List getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }
    public List displayAllFamilies(){
        return collectionFamilyDao.getAllFamilies();
    }
    public List getFamiliesBiggerThan(int quantity){
        List<Family> listBiggerFamily = new ArrayList<>();
        for (int i = 0; i < collectionFamilyDao.getAllFamilies().size(); i++) {
            if (collectionFamilyDao.getFamilyByIndex(i).countFamily() > quantity) {
                listBiggerFamily.add(collectionFamilyDao.getFamilyByIndex(i));
            }
        }
        System.out.println(listBiggerFamily);
        return listBiggerFamily;
    }
    public List getFamiliesLessThan(int quantity) {
        List<Family> listLessFamily = new ArrayList<>();
        for (int i = 0; i < collectionFamilyDao.getAllFamilies().size(); i++) {
            if (collectionFamilyDao.getFamilyByIndex(i).countFamily() < quantity) {
                listLessFamily.add(collectionFamilyDao.getFamilyByIndex(i));
            }
        }
        System.out.println(listLessFamily);
        return listLessFamily;
    }


    public int countFamiliesWithMemberNumber(int quantity) {
        int index = 0;
        for (int i = 0; i < collectionFamilyDao.getAllFamilies().size(); i++) {
            if (collectionFamilyDao.getFamilyByIndex(i).countFamily() == quantity) {
                index++;
            }
        }
        System.out.println(index);
        return index;
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother,father);
        collectionFamilyDao.saveFamily(family);
    }
    public Family getFamilyByIndex(int index) {
        return collectionFamilyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return collectionFamilyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }


    public void saveFamily(Family family) {
        collectionFamilyDao.saveFamily(family);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            Woman girl = new Woman();
            girl.setName(womanName);
            girl.setFamily(family.getFather().getFamily());
            girl.setSurname(family.getFather().getSurname());
            girl.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(girl);
        } else {
            Man boy = new Man();
            boy.setName(manName);
            boy.setFamily(family.getFather().getFamily());
            boy.setSurname(family.getFather().getSurname());
            boy.setIq((family.getFather().getIq() + family.getMother().getIq()) / 2);
            family.addChild(boy);
        }
        saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int year) {
        for (int i = 0; i < collectionFamilyDao.getAllFamilies().size(); i++) {
            for (int j = 0; j < collectionFamilyDao.getFamilyByIndex(i).getChildren().size(); j++) {
                if (collectionFamilyDao.getFamilyByIndex(i).getChildren().get(j).getYear() < year) {
                    collectionFamilyDao.getFamilyByIndex(i).deleteChild(j);
                    j--;
                }
            }
        }
    }

    public int count() {
        return collectionFamilyDao.getAllFamilies().size();
    }

    public HashSet<Pet> getPets (int index) {
        return collectionFamilyDao.getFamilyByIndex(index).getPetsSet();
    }


    public void addPet(int indexOfFamily, Pet pet) {
        HashSet<Pet> petHashSet = new HashSet<>();
        if (collectionFamilyDao.getFamilyByIndex(indexOfFamily).getPetsSet() == null){
            collectionFamilyDao.getFamilyByIndex(indexOfFamily).setPetsSet(petHashSet);
        }
        collectionFamilyDao.getFamilyByIndex(indexOfFamily).getPetsSet().add(pet);
    }



}



