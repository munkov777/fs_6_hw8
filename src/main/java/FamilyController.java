import java.util.List;
import java.util.HashSet;
import java.util.List;


public class FamilyController {
        private FamilyService familyService;

        public FamilyController(FamilyService familyService) {
            this.familyService = familyService;
        }

        public FamilyController() {
        }

        public void setFamilyService(FamilyService familyService) {
            this.familyService = familyService;
        }

        public List getAllFamilies() {
            return familyService.getAllFamilies();
        }

        public void displayAllFamilies() {
            System.out.println(familyService.getAllFamilies());;
        }

        public List getFamiliesBiggerThan(int quantity) {
            return familyService.getFamiliesBiggerThan(quantity);
        }

        public List getFamiliesLessThan(int quantity) {
            return familyService.getFamiliesLessThan(quantity);
        }

        public int countFamiliesWithMemberNumber(int quantity) {
            return familyService.countFamiliesWithMemberNumber(quantity);
        }

        public void createNewFamily(Human mother, Human father) {
            familyService.createNewFamily(mother,father);
        }

       public Family bornChild(Family family, String manName, String womanName) {
            return familyService.bornChild(family,manName,womanName);
       }

        public Family adoptChild(Family family, Human child) {
            return familyService.adoptChild(family, child);
        }

        public void deleteAllChildrenOlderThen(int year) {
            familyService.deleteAllChildrenOlderThen(year);
        }

        public int count() {
            return familyService.count();
        }

        public HashSet<Pet> getPets (int index) {
            return familyService.getCollectionFamilyDao().getFamilyByIndex(index).getPetsSet();
        }

        public void addPet(int indexOfFamily, Pet pet) {
            familyService.getCollectionFamilyDao().addPet(indexOfFamily,pet);
        }
    }



